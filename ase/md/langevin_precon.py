"""Langevin dynamics class."""


import numpy as np
from numpy.random import standard_normal, seed
from ase.units import fs
from ase.md.md import MolecularDynamics
from ase.parallel import world
from scipy import sparse
from scipy.sparse.linalg import expm, spsolve
from scipy.linalg import sqrtm, cholesky, solve 

try:
    from scikits.sparse.cholmod import cholesky as spcholesky
    have_sparse_cholesky = True
except ImportError:
    have_sparse_cholesky = False

def remove_com_trans_rot(atoms):
    """
    Remove COM translational and rotational velocities
    """

    # compute COM coordinate
    masses = atoms.get_masses()
    total_mass = np.sum(masses)
    xcom = atoms.get_center_of_mass()

    # compute COM translational momentum, velocity and kinetic energy
    momenta = atoms.get_momenta()
    trans_mom_com = np.zeros(3)
    for i in range(len(atoms)):
        trans_mom_com += momenta[i]
    trans_vel_com = trans_mom_com / total_mass
    trans_kin = 0.5 * np.dot(trans_vel_com, trans_vel_com) * total_mass

    # compute COM angular momentum, velocity and kinetic energy
    ang_mom_com = atoms.get_angular_momentum()
    inertia_tensor = np.zeros((3,3))
    for i in range(len(atoms)):
        rpos = atoms.get_positions()[i,:] - xcom
        inertia_tensor[0,0] += masses[i] * (rpos[1]**2 + rpos[2]**2)
        inertia_tensor[1,1] += masses[i] * (rpos[0]**2 + rpos[2]**2)
        inertia_tensor[2,2] += masses[i] * (rpos[0]**2 + rpos[1]**2)
        inertia_tensor[0,1] -= masses[i] * (rpos[0]*rpos[1])
        inertia_tensor[0,2] -= masses[i] * (rpos[0]*rpos[2])
        inertia_tensor[1,2] -= masses[i] * (rpos[1]*rpos[2])
    inertia_tensor[1,0] = inertia_tensor[0,1]
    inertia_tensor[2,0] = inertia_tensor[0,2]
    inertia_tensor[2,1] = inertia_tensor[1,2]
    ang_vel_com = np.linalg.solve(inertia_tensor, ang_mom_com)
    rot_kin = 0.5 * np.dot(ang_vel_com, ang_mom_com)

    print("Removing COM translational and rotational velocities")
    print("KE(trans) = ", trans_kin, "KE(rot) = ", rot_kin) 
    
    for i in range(len(atoms)):
        momenta[i] -= masses[i] * trans_vel_com

    for i in range(len(atoms)):
        rpos = atoms.get_positions()[i,:] - xcom
        momenta[i] -= masses[i] * (np.cross(ang_vel_com, rpos))

    atoms.set_momenta(momenta)

class BBK(MolecularDynamics):
    """Langevin (constant N, V, T) molecular dynamics using the BBK scheme.

    Usage: Langevin(atoms, dt, temperature, friction)

    atoms
        The list of atoms.
        
    dt
        The time step.

    temperature
        The desired temperature, in energy units.

    friction
        A friction coefficient in ps^-1.

    This dynamics accesses the atoms using Cartesian coordinates."""

    def __init__(self, atoms, timestep, temperature, friction,
                 trajectory=None, logfile=None, loginterval=1,
                 nscm=0, precon=None, use_cholesky=False, 
                 nprecon_update=1, communicator=world):
        MolecularDynamics.__init__(self, atoms, timestep, trajectory,
                                   logfile, loginterval)
        self.temp = temperature
        self.frict = friction / (1000*fs)
        self.nscm = nscm
        self.precon = precon
        self.use_cholesky = use_cholesky
        self.nprecon_update = np.int(nprecon_update)
        if self.nprecon_update < 1:
            print("nprecon_update is less than 1 => nprecon_update = 1")
            self.nprecon_update = 1
        self.P_matrix = None
        self.P_matrix_M_inv = None
        self.P_matrix_half = None
        self.P_matrix_half_r = None
        self.communicator = communicator

    def step(self, force):
        f = np.reshape(force, -1)
        p = np.reshape(self.atoms.get_momenta(), -1)
        q = np.reshape(self.atoms.get_positions(), -1)
        m = np.repeat(np.reshape(self.masses, -1), 3)
        r = standard_normal(3*len(self.atoms))

        if self.precon is not None:
            # p(n+1/2)
            if (self.nsteps+1)%self.nprecon_update == 0 or self.nsteps == 0:
                self.P_matrix = self.precon.make_precon(self.atoms)
                if hasattr(self.P_matrix, 'todense'):
                    M_inv = sparse.csc_matrix(np.eye(3*len(self.atoms))/m)
                else:
                    M_inv = np.eye(3*len(self.atoms))/m
                self.P_matrix_M_inv = self.P_matrix.dot(M_inv)
                if self.use_cholesky:
                    if have_sparse_cholesky and hasattr(self.P_matrix, 'todense'):
                        self.P_matrix_half = spcholesky(self.P_matrix).L()
                    else:
                        if hasattr(self.P_matrix, 'todense'):
                            self.P_matrix_half = cholesky(self.P_matrix.todense(), lower=True)
                        else:
                            self.P_matrix_half = cholesky(self.P_matrix, lower=True)
                else:
                    if hasattr(self.P_matrix, 'todense'):
                        self.P_matrix_half = np.real(sqrtm(((self.P_matrix+self.P_matrix.T)/2.0).todense()))
                    else:
                        self.P_matrix_half = np.real(sqrtm(((self.P_matrix+self.P_matrix.T)/2.0)))
            self.P_matrix_half_r = self.P_matrix_half.dot(r)
            p = p - self.dt/2.0*self.P_matrix_M_inv.dot(p) + self.dt/2.0*f + np.sqrt(self.dt/2.0*self.temp)*self.P_matrix_half_r
            # q(n+1)
            q = q + self.dt*p/m
            # p(n+1)
            self.atoms.set_positions(np.reshape(q, (len(self.atoms), 3)))
            force = self.atoms.get_forces()
            f = np.reshape(force, -1)
            if hasattr(self.P_matrix, 'todense'):
                p = spsolve(sparse.csc_matrix(np.eye(3*len(self.atoms))+self.dt/2.0*self.P_matrix_M_inv), p + self.dt/2.0*f + np.sqrt(self.dt/2.0*self.temp)*self.P_matrix_half_r)
            else:
                p = solve(np.eye(3*len(self.atoms))+self.dt/2.0*self.P_matrix_M_inv, p + self.dt/2.0*f + np.sqrt(self.dt/2.0*self.temp)*self.P_matrix_half_r)
            self.atoms.set_momenta(np.reshape(p, (len(self.atoms), 3)))
        else:
            # p(n+1/2)
            p = (1.0 - self.dt/2.0*self.frict)*p + self.dt/2.0*f + np.sqrt(self.dt/2.0*self.temp*self.frict)*m**0.5*r
            # q(n+1)
            q = q + self.dt*p/m
            # p(n+1)
            self.atoms.set_positions(np.reshape(q, (len(self.atoms), 3)))
            force = self.atoms.get_forces()
            f = np.reshape(force, -1)
            p = (p + self.dt/2.0*f + np.sqrt(self.dt/2.0*self.temp*self.frict)*m**0.5*r)/(1.0 + self.dt/2.0*self.frict)
            self.atoms.set_momenta(np.reshape(p, (len(self.atoms), 3)))

        if self.nscm != 0:
            if (self.nsteps+1)%self.nscm == 0 or self.nsteps == 0:
                remove_com_trans_rot(self.atoms)

        return force

class BAOAB(MolecularDynamics):
    """Langevin (constant N, V, T) molecular dynamics using the BAOAB scheme.

    Usage: Langevin(atoms, dt, temperature, friction)

    atoms
        The list of atoms.
        
    dt
        The time step.

    temperature
        The desired temperature, in energy units.

    friction
        A friction coefficient in ps^-1.

    This dynamics accesses the atoms using Cartesian coordinates."""

    def __init__(self, atoms, timestep, temperature, friction,
                 trajectory=None, logfile=None, loginterval=1,
                 nscm=0, precon=None, use_cholesky=False, 
                 rndseed=None, communicator=world):
        MolecularDynamics.__init__(self, atoms, timestep, trajectory,
                                   logfile, loginterval)
        self.temp = temperature
        self.frict = friction / (1000*fs)
        self.nscm = nscm
        self.precon = precon
        self.use_cholesky = use_cholesky
        self.communicator = communicator

        if rndseed is not None:
            seed(rndseed)

    def step(self, force):
        
        f = np.reshape(force, -1)
        p = np.reshape(self.atoms.get_momenta(), -1)
        q = np.reshape(self.atoms.get_positions(), -1)
        m = np.repeat(np.reshape(self.masses, -1), 3)
        r = standard_normal(3*len(self.atoms))
        e = np.exp(-self.dt*self.frict)
        chi2 = (self.temp*(1.0-e*e))**0.5
        M_inv = sparse.csc_matrix(np.eye(3*len(self.atoms))/m)
#       M_half_inv = sparse.csc_matrix(np.eye(3*len(self.atoms))/m**0.5)

        if self.precon is not None:
            # B
            p = p + self.dt/2.0*f
            # A
            q = q + self.dt/2.0*p/m
            # O
#           P_matrix = M_half_inv.dot(self.precon.make_precon(self.atoms).tocsc().dot(M_half_inv))
            P_matrix = self.precon.make_precon(self.atoms).tocsc()
            e = expm(-self.dt*self.frict*P_matrix.dot(M_inv))
            e2 = expm(-2.0*self.dt*self.frict*P_matrix)
            if self.use_cholesky:
                if have_sparse_cholesky:
                    chi2 = spcholesky(self.temp*(sparse.csc_matrix(np.eye(3*len(self.atoms)))-e2)).L()
                else:
                    chi2 = cholesky(self.temp*(sparse.csc_matrix(np.eye(3*len(self.atoms)))-e2), lower=True)
            else:
                chi2 = np.real(sqrtm(self.temp*(np.eye(3*len(self.atoms))-e2.todense())))
            p = e.dot(p) + chi2.dot(r)
            # A
            q = q + self.dt/2.0*p/m
            # B
            self.atoms.set_positions(np.reshape(q, (len(self.atoms), 3)))
            force = self.atoms.get_forces()
            f = np.reshape(force, -1)
            p = p + self.dt/2.0*f
            self.atoms.set_momenta(np.reshape(p, (len(self.atoms), 3)))
        else:
            # B
            p = p + self.dt/2.0*f
            # A
            q = q + self.dt/2.0*p/m
            # O
            p = e*p+chi2*m**0.5*r
            # A
            q = q + self.dt/2.0*p/m
            # B
            self.atoms.set_positions(np.reshape(q, (len(self.atoms), 3)))
            force = self.atoms.get_forces()
            f = np.reshape(force, -1)
            p = p + self.dt/2.0*f
            self.atoms.set_momenta(np.reshape(p, (len(self.atoms), 3)))

        if self.nscm != 0:
            if (self.nsteps+1)%self.nscm == 0 or self.nsteps == 0:
                remove_com_trans_rot(self.atoms)

        return force
