from __future__ import division

import numpy as np

from ase.calculators.calculator import Calculator


class ForceField(Calculator):
    implemented_properties = ['energy', 'forces', 'hessian', 'energies']
    nolabel = True

    def __init__(self, terms=None, **kwargs):
        Calculator.__init__(self, **kwargs)
        self.terms = terms

    def calculate(self, atoms, properties, system_changes):
        Calculator.calculate(self, atoms, properties, system_changes)
        if system_changes:
            for name in ['energy', 'forces', 'hessian', 'energies']:
                self.results.pop(name, None)
        if 'energy' in properties and 'energy' not in self.results:
            energy = 0.0
            for term in self.terms:
                e = term.get_potential_value(atoms)
                energy += e
            self.results['energy'] = energy
        if 'forces' in properties and 'forces' not in self.results:
            forces = np.zeros(3 * len(atoms))
            for term in self.terms:
                idx = term.get_coord_indexes()
                g = term.get_potential_gradient(atoms)
                forces[idx] -= g
            self.results['forces'] = np.reshape(forces, (len(atoms), 3))
        if 'hessian' in properties and 'hessian' not in self.results:
            hessian = np.zeros((3 * len(atoms), 3 * len(atoms)))
            for term in self.terms:
                idx = term.get_coord_indexes()
                h = term.get_potential_hessian(atoms)
                for li, gi in enumerate(idx):
                    hessian[gi, idx] += h[li, :]
            self.results['hessian'] = hessian
        if 'energies' in properties and 'energies' not in self.results:
            energies = np.zeros(len(atoms))
            for term in self.terms:
                e = term.get_potential_value(atoms)
                energies[term.get_atom_indexes] += e
            self.results['energies'] = energies

    def get_hessian(self, atoms=None):
        return self.get_property('hessian', atoms)

    def get_potential_energies(self, atoms=None):
        return self.get_property('energies', atoms)
