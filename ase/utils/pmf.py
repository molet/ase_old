import numpy as np

from ase.calculators.calculator import Calculator
from scipy import sparse 

class ICF:

    def __init__(self, atoms=None, cvs=None, T=None, Gw_type='G'):

        if cvs is None:
            raise ValueError("cvs list must be defined!")
        elif type(cvs) is not list:
            raise ValueError("cvs must be a list!")
        else:
            self.cvs = cvs
            self.dim = len(cvs)

        if T is None:
            raise ValueError("T must be defined!")
        else:
            self.T = T

        self.Gw_type = Gw_type

        self.atom_indexes = []
        self.common_atom_indexes = [[[] for i in range(self.dim)] for j in range(self.dim)]

        for cvi in range(self.dim):
            self.atom_indexes.append(self.cvs[cvi].get_atom_indexes())

        for cvi in range(self.dim):
            for li, ai in enumerate(self.atom_indexes[cvi]):
                self.common_atom_indexes[cvi][cvi].append([ai, li, li])
            for cvj in range(cvi+1, self.dim):
                for aij in list(set(self.atom_indexes[cvi]).intersection(self.atom_indexes[cvj])):
                    li = self.atom_indexes[cvi].index(aij)
                    lj = self.atom_indexes[cvj].index(aij)
                    self.common_atom_indexes[cvi][cvj].append([aij, li, lj])
                    self.common_atom_indexes[cvj][cvi].append([aij, lj, li])

    def calculate(self, atoms):

        gx = []
        Hx = []

        for cvi in range(self.dim):
            gx.append(self.cvs[cvi].get_gradient(atoms))
            Hx.append(self.cvs[cvi].get_hessian(atoms))

        if 'pot_forces' in atoms._calc.implemented_properties:
            f = atoms._calc.get_pot_forces(atoms).reshape(-1)
        else:
            f = atoms._calc.get_forces(atoms).reshape(-1)

        icf = np.zeros(self.dim)
        W = []
        dW_dx = []
        nabla_W = np.zeros(self.dim)
        Gw = np.zeros((self.dim, self.dim))
        if self.Gw_type == 'G':
            for cvi in range(self.dim):
                W.append(gx[cvi])
                dW_dx.append(Hx[cvi])
        elif self.Gw_type == 'Z':
            for cvi in range(self.dim):
                masses_inv = 1.0/atoms.get_masses()[self.cvs[cvi].get_atom_indexes()].repeat(3)
                W.append(gx[cvi]*masses_inv)
                dW_dx.append(Hx[cvi]*masses_inv)
        elif self.Gw_type == 'E':
            for cvi in range(self.dim):
                W.append(gx[cvi]*np.exp(-gx[cvi]**2/2.0))
                dW_dx.append(Hx[cvi]*np.exp(-gx[cvi]**2/2.0)*(1.0-gx[cvi]**2))
        elif self.Gw_type == 'S':
                W.append(1.0/(1.0+np.exp(-gx[cvi]))-0.5)
                dW_dx.append(Hx[cvi]/(1+np.exp(-gx[cvi]))**2*np.exp(-gx[cvi]))
        elif self.Gw_type == 'R':
                np.random.seed(314159265)
                W.append(np.random.uniform(-1.,1.,len(gx[cvi])))
                dW_dx.append(np.zeros((len(gx[cvi]), len(gx[cvi]))))
        elif self.Gw_type == 'F':
                W.append(f[self.cvs[cvi].get_coord_indexes()])
                dW_dx.append(np.zeros((len(gx[cvi]), len(gx[cvi]))))
        elif self.Gw_type == 'T':
            for cvi in range(self.dim):
                W.append(np.sin(gx[cvi]))
                dW_dx.append(Hx[cvi]*np.cos(gx[cvi]))
        else:
            raise NotImplementedError("not implemented GramType")

        for cvi in range(self.dim):
            nabla_W[cvi] = np.sum(dW_dx[cvi].diagonal())
            Gw[cvi, cvi] = np.dot(W[cvi], gx[cvi])
            for cvj in range(cvi+1, self.dim):
                for aij, li, lj in self.common_atom_indexes[cvi][cvj]:
                    Gw[cvi, cvj] += np.dot(W[cvi][3*li:3*li+3], gx[cvj][3*lj:3*lj+3])
                Gw[cvj, cvi] = Gw[cvi, cvj]
        Gw_inv = np.linalg.inv(Gw)
        for cvi in range(self.dim):
            div = 0.0
            for cvj in range(self.dim):
                for lj, aj in enumerate(self.atom_indexes[cvj]):
                    icf[cvi] -= Gw_inv[cvi, cvj] * np.dot(W[cvj][3*lj:3*lj+3], f[3*aj:3*aj+3])
                nabla_Gw_inv = np.zeros(len(W[cvj]))
                for lj, aj in enumerate(self.atom_indexes[cvj]):
                    for cvk in range(self.dim):
                        for cvl in range(self.dim):
                            nabla_Gw = np.zeros(3)
                            if aj in self.atom_indexes[cvk]:
                                lkj = self.atom_indexes[cvk].index(aj)
                                for akl, lk, ll in self.common_atom_indexes[cvk][cvl]:
                                    nabla_Gw[:] += np.dot(dW_dx[cvk][3*lkj:3*lkj+3, 3*lk:3*lk+3], gx[cvl][3*ll:3*ll+3])
                            if aj in self.atom_indexes[cvl]:
                                llj = self.atom_indexes[cvl].index(aj)
                                for alk, ll, lk in self.common_atom_indexes[cvl][cvk]:
                                    nabla_Gw[:] += np.dot(Hx[cvl][3*llj:3*llj+3, 3*ll:3*ll+3], W[cvk][3*lk:3*lk+3])
                            nabla_Gw_inv[3*lj:3*lj+3] -= Gw_inv[cvi, cvk] * Gw_inv[cvl, cvj] * nabla_Gw[:]
                div += np.dot(nabla_Gw_inv, W[cvj]) + Gw_inv[cvi, cvj] * nabla_W[cvj]
            icf[cvi] -= self.T * div

        return icf

class ABF(Calculator):
    implemented_properties=['energy', 'forces', 'pot_energy', 'pot_forces', 'abf_energy', 'abf_forces']

    def __init__(self, atoms=None, calc=None, label=None, cvs=None, nbins=None, nmin=None, abfmin=None, abfmax=None, T=None, abfstep=None, abffile='abf.txt', Gw_type='G'):
        Calculator.__init__(self, label, atoms)
        self.calc = calc

        if cvs is None:
            raise ValueError("cvs list must be defined!")
        elif type(cvs) is not list:
            raise ValueError("cvs must be a list!")
        else:
            self.cvs = cvs
            self.dim = len(cvs)

        if nbins is None:
            raise ValueError("nbins list must be defined!")
        elif type(nbins) is not list:
            raise ValueError("nbins must be a list!")
        elif len(nbins) != self.dim:
            raise ValueError("size of nbins and cvs is not equal!")
        else:
            self.nbins = np.array(nbins)
            self.bins = sparse.lil_matrix((1,np.prod(self.nbins)), dtype=int)
            self.accs = sparse.lil_matrix((self.dim,np.prod(self.nbins)), dtype=float)

        if nmin is None:
            raise ValueError("nmin must be defined!")
        else:
            self.nmin = nmin

        if abfmin is None:
            raise ValueError("abfmin list must be defined!")
        elif type(abfmin) is not list:
            raise ValueError("abfmin must be a list!")
        elif len(abfmin) != self.dim:
            raise ValueError("size of abfmin and cvs is not equal!")
        else:
            self.abfmin = np.array(abfmin)

        if abfmax is None:
            raise ValueError("abfmax list must be defined!")
        elif type(abfmax) is not list:
            raise ValueError("abfmax must be a list!")
        elif len(abfmax) != self.dim:
            raise ValueError("size of abfmax and cvs is not equal!")
        else:
            self.abfmax = np.array(abfmax)

        self.binwidths = np.zeros(self.dim)
        for cv in range(self.dim):
            self.binwidths[cv] = (self.abfmax[cv] - self.abfmin[cv])/self.nbins[cv]

        if T is None:
            raise ValueError("T must be defined!")
        else:
            self.T = T

        if abfstep is None:
            raise ValueError("abfstep must be defined!")
        elif abfstep <= 0:
            raise ValueError("abfstep must be larger than 0!")
        else:
            self.abfstep = abfstep

        if abffile != '-':
            self.abffile = open(abffile, 'w')
        else:
            self.abffile = None

        self.nsteps = -1

        self.nbin = -1
        self.values = np.zeros(self.dim)
        self.ICF = ICF(atoms=atoms, cvs=cvs, T=T, Gw_type=Gw_type)
        self.icfs = np.zeros(self.dim)

    def calculate(self, atoms, properties, system_changes):
        Calculator.calculate(self, atoms, properties, system_changes)
        if system_changes:
            if 'energy' in self.results:
                del self.results['energy']
            if 'forces' in self.results:
                del self.results['forces']
            if 'pot_energy' in self.results:
                del self.results['pot_energy']
            if 'pot_forces' in self.results:
                del self.results['pot_forces']
            if 'abf_energy' in self.results:
                del self.results['abf_energy']
            if 'abf_forces' in self.results:
                del self.results['abf_forces']

            self.nsteps += 1

            a = atoms.copy()
            a.set_calculator(self.calc)
            e = a.get_potential_energy()
            f = a.get_forces().reshape(-1)
            fb = np.zeros(3*len(atoms))

            e_abf = 0.0
            f_abf = np.zeros(self.dim)

            self.nbin = 0
            for cv in range(self.dim):
                self.values[cv] = self.cvs[cv].get_value(atoms)
                n = np.int((self.values[cv] - self.abfmin[cv]) / self.binwidths[cv])
                if n < 0 or n > self.nbins[cv]:
                    self.nbin = -1
                    break
                self.nbin += n * np.prod(self.nbins[:cv])

            if self.nbin != -1:
                n = self.bins[0,self.nbin]
                if n > self.nmin:
                    f_abf = (self.accs[:,self.nbin] / n).toarray()
                elif n > 0:
                    f_abf = (self.accs[:,self.nbin] / n).toarray() * n / self.nmin

            self.results['pot_energy'] = e
            self.results['abf_energy'] = e_abf
            self.results['energy'] = self.results['pot_energy'] + self.results['abf_energy']

            self.results['pot_forces'] = f.reshape((-1,3))
            for cv in range(self.dim):
                coord_indexes = self.cvs[cv].get_coord_indexes()
                fb[coord_indexes] += f_abf[cv] * self.cvs[cv].get_gradient(atoms)
            self.results['abf_forces'] = fb.reshape((-1,3))
            self.results['forces'] = self.results['pot_forces'] + self.results['abf_forces']

            if self.nsteps != 0 and self.nsteps%self.abfstep == 0:
                self.history(atoms)

    def history(self, atoms):

        self.icfs = self.ICF.calculate(atoms)

        if self.nbin != -1:
            self.bins[0,self.nbin] += 1
            for cv in range(self.dim):
                self.accs[cv,self.nbin] += self.icfs[cv]

        if self.abffile is None:
            return

        line = ''
        line += '%12d' % self.nsteps
        for cv in range(self.dim):
            line += '%15.8f' % self.values[cv]
        for cv in range(self.dim):
            line += '%15.8f' % self.icfs[cv]
        line += '\n'

        self.abffile.write(line)
        self.abffile.flush()

    def get_pot_energy(self, atoms=None):
        return self.get_property('pot_energy', atoms)

    def get_pot_forces(self, atoms=None):
        return self.get_property('pot_forces', atoms)

    def get_abf_energy(self, atoms=None):
        return self.get_property('abf_energy', atoms)

    def get_abf_forces(self, atoms=None):
        return self.get_property('abf_forces', atoms)


class US(Calculator):
    implemented_properties=['energy', 'forces', 'pot_energy', 'pot_forces', 'us_energy', 'us_forces']

    def __init__(self, atoms=None, calc=None, label=None, cvs=None, k=None, usval=None, usside=None, usinc=None, usstep=None, usfile='us.txt'):
        Calculator.__init__(self, label, atoms)
        self.calc = calc

        if cvs is None:
            raise ValueError("cvs list must be defined!")
        elif type(cvs) is not list:
            raise ValueError("cvs must be a list!")
        else:
            self.cvs = cvs
            self.dim = len(cvs)

        if k is None:
            raise ValueError("k list must be defined!")
        elif type(k) is not list:
            raise ValueError("k must be a list!")
        elif len(k) != self.dim:
            raise ValueError("size of k and cvs is not equal!")
        else:
            self.k = np.array(k)

        if usval is None:
            raise ValueError("usval list must be defined!")
        elif type(usval) is not list:
            raise ValueError("usval must be a list!")
        elif len(usval) != self.dim:
            raise ValueError("size of usval and cvs is not equal!")
        else:
            self.usval = np.array(usval)

        if usside is None:
            self.usside = np.zeros(self.dim, dtype=np.int)
        elif type(usside) is not list:
            raise ValueError("usside must be a list!")
        elif len(usside) != self.dim:
            raise ValueError("size of usside and cvs is not equal!")
        else:
            self.usside = np.array(usside)

        if usinc is None:
            raise ValueError("usinc list must be defined!")
        elif type(usinc) is not list:
            raise ValueError("usinc must be a list!")
        elif len(usinc) != self.dim:
            raise ValueError("size of usinc and cvs is not equal!")
        else:
            self.usinc = np.array(usinc)

        if usstep is None:
            raise ValueError("usstep must be defined!")
        elif usstep <= 0:
            raise ValueError("usstep must be larger than 0!")
        else:
            self.usstep = usstep

        if usfile != '-':
            self.usfile = open(usfile, 'w')
        else:
            self.usfile = None

        self.nsteps = -1

        self.values = np.zeros(self.dim)

    def calculate(self, atoms, properties, system_changes):
        Calculator.calculate(self, atoms, properties, system_changes)
        if system_changes:
            if 'energy' in self.results:
                del self.results['energy']
            if 'forces' in self.results:
                del self.results['forces']
            if 'pot_energy' in self.results:
                del self.results['pot_energy']
            if 'pot_forces' in self.results:
                del self.results['pot_forces']
            if 'us_energy' in self.results:
                del self.results['us_energy']
            if 'us_forces' in self.results:
                del self.results['us_forces']

            self.nsteps += 1

            a = atoms.copy()
            a.set_calculator(self.calc)
            e = a.get_potential_energy()
            f = a.get_forces().reshape(-1)
            fb = np.zeros(3*len(atoms))

            e_us = 0.0
            f_us = np.zeros(self.dim)

            diff = np.zeros(self.dim)
            for cv in range(self.dim):
                self.values[cv] = self.cvs[cv].get_value(atoms)
                restraining_value = self.cvs[cv].get_sum(self.usval[cv], self.usinc[cv]*self.nsteps)
                diff[cv] = self.cvs[cv].get_dev(self.values[cv], restraining_value)
                if self.usside[cv] < 0 and diff[cv] > 0: 
                   diff[cv] = 0.0
                elif self.usside[cv] > 0 and diff[cv] < 0:
                   diff[cv] = 0.0
            e_us = 0.5*np.dot(self.k, diff**2)
            f_us = -self.k*diff

            self.results['pot_energy'] = e
            self.results['us_energy'] = e_us
            self.results['energy'] = self.results['pot_energy'] + self.results['us_energy']

            self.results['pot_forces'] = f.reshape((-1,3))
            for cv in range(self.dim):
                coord_indexes = self.cvs[cv].get_coord_indexes()
                fb[coord_indexes] += f_us[cv] * self.cvs[cv].get_gradient(atoms)
            self.results['us_forces'] = fb.reshape((-1,3))
            self.results['forces'] = self.results['pot_forces'] + self.results['us_forces']

            if self.nsteps != 0 and self.nsteps%self.usstep == 0:
                self.history(atoms)

    def history(self, atoms):

        if self.usfile is None:
            return

        line = ''
        line += '%12d' % self.nsteps
        for cv in range(self.dim):
            line += '%15.8f' % self.values[cv]
        line += '\n'

        self.usfile.write(line)
        self.usfile.flush()

    def get_pot_energy(self, atoms=None):
        return self.get_property('pot_energy', atoms)

    def get_pot_forces(self, atoms=None):
        return self.get_property('pot_forces', atoms)

    def get_us_energy(self, atoms=None):
        return self.get_property('us_energy', atoms)

    def get_us_forces(self, atoms=None):
        return self.get_property('us_forces', atoms)

class MTD(Calculator):
    implemented_properties=['energy', 'forces', 'pot_energy', 'pot_forces', 'mtd_energy', 'mtd_forces']

    def __init__(self, atoms=None, calc=None, label=None, cvs=None, height=None, widths=None, mtdmin=None, mtdmax=None, mtdtemp=None, mtdstep=None, mtdab=False, mtdfile='mtd.txt'):
        Calculator.__init__(self, label, atoms)
        self.calc = calc

        if cvs is None:
            raise ValueError("cvs list must be defined!")
        elif type(cvs) is not list:
            raise ValueError("cvs must be a list!")
        else:
            self.cvs = cvs
            self.dim = len(cvs)

        if height is None:
            raise ValueError("height must be defined!")
        else:
            self.height = height

        if widths is None:
            raise ValueError("widths list must be defined!")
        elif type(widths) is not list:
            raise ValueError("widths must be a list!")
        elif len(widths) != self.dim:
            raise ValueError("size of widths and cvs is not equal!")
        else:
            self.widths = np.array(widths)
            self.widths2 = self.widths**2

        if mtdmin is None:
            raise ValueError("mtdmin list must be defined!")
        elif type(mtdmin) is not list:
            raise ValueError("mtdmin must be a list!")
        elif len(mtdmin) != self.dim:
            raise ValueError("size of mtdmin and cvs is not equal!")
        else:
            self.mtdmin = np.array(mtdmin)

        if mtdmax is None:
            raise ValueError("mtdmax list must be defined!")
        elif type(mtdmax) is not list:
            raise ValueError("mtdmax must be a list!")
        elif len(mtdmax) != self.dim:
            raise ValueError("size of mtdmax and cvs is not equal!")
        else:
            self.mtdmax = np.array(mtdmax)

        self.mtdtemp = mtdtemp

        if mtdstep is None:
            raise ValueError("mtdstep must be defined!")
        elif mtdstep <= 0:
            raise ValueError("mtdstep must be larger than 0!")
        else:
            self.mtdstep = mtdstep

        self.mtdab = mtdab

        if mtdfile != '-':
            self.mtdfile = open(mtdfile, 'w')
        else:
            self.mtdfile = None

        self.mtd_energy = 0
        self.nsteps = -1

        self.values = np.zeros(self.dim)
        self.history_values = []
        self.history_height = []

        self.history_k_lower = []
        self.history_k_upper = []

        self.domain = True

    def calculate(self, atoms, properties, system_changes):
        Calculator.calculate(self, atoms, properties, system_changes)
        if system_changes:
            if 'energy' in self.results:
                del self.results['energy']
            if 'forces' in self.results:
                del self.results['forces']
            if 'pot_energy' in self.results:
                del self.results['pot_energy']
            if 'pot_forces' in self.results:
                del self.results['pot_forces']
            if 'mtd_energy' in self.results:
                del self.results['mtd_energy']
            if 'mtd_forces' in self.results:
                del self.results['mtd_forces']

            self.nsteps += 1

            a = atoms.copy()
            a.set_calculator(self.calc)
            e = a.get_potential_energy()
            f = a.get_forces().reshape(-1)
            fb = np.zeros(3*len(atoms))

            e_mtd = 0.0
            f_mtd = np.zeros(self.dim)
            for cv in range(self.dim):
                self.values[cv] = self.cvs[cv].get_value(atoms)

            self.domain = True
            if not np.all(self.mtdmin <= self.values):
                self.domain = False
            elif not np.all(self.mtdmax >= self.values):
                self.domain = False

            if self.domain and self.mtdab:
                for values, height, k_lower, k_upper in zip(self.history_values, self.history_height, self.history_k_lower, self.history_k_upper):
                    diff = np.zeros(self.dim)
                    k = np.zeros(self.dim)
                    for cv in range(self.dim):
                        diff[cv] = self.cvs[cv].get_dev(self.values[cv], values[cv])
                        if diff[cv] < 0:
                            k[cv] = k_lower[cv]
                        else:
                            k[cv] = k_upper[cv] 
                    diff_per_widths2 = diff / self.widths2
                    fexp = np.exp(-0.5*diff*diff_per_widths2)
                    pots = fexp + 0.5*k*diff**2
                    fors = fexp*diff_per_widths2 - k*diff
                    e_mtd += height*np.prod(pots)
                    for cv in range(self.dim):
                        f_mtd[cv] += height*np.prod(pots[:cv])*fors[cv]*np.prod(pots[cv+1:])

            elif self.domain:
                for values, height in zip(self.history_values, self.history_height):
                    diff = np.zeros(self.dim)
                    for cv in range(self.dim):
                        diff[cv] = self.cvs[cv].get_dev(self.values[cv], values[cv]) 
                    diff_per_widths2 = diff / self.widths2
                    fexp = height*np.exp(-0.5*np.dot(diff_per_widths2, diff))
                    e_mtd += fexp
                    f_mtd += fexp * diff_per_widths2

            self.results['pot_energy'] = e
            self.results['mtd_energy'] = e_mtd
            self.results['energy'] = self.results['pot_energy'] + self.results['mtd_energy']

            self.results['pot_forces'] = f.reshape((-1,3))
            for cv in range(self.dim):
                coord_indexes = self.cvs[cv].get_coord_indexes()
                fb[coord_indexes] += f_mtd[cv] * self.cvs[cv].get_gradient(atoms)
            self.results['mtd_forces'] = fb.reshape((-1,3))
            self.results['forces'] = self.results['pot_forces'] + self.results['mtd_forces']

            if self.nsteps != 0 and self.nsteps%self.mtdstep == 0 and self.domain:
                self.history(atoms)

    def history(self, atoms):

        self.history_values.append(self.values.copy())
        if self.mtdtemp is None:
            height = self.height
        else:
            height = self.height*np.exp(-self.get_property('mtd_energy', atoms)/self.mtdtemp)
        self.history_height.append(height)

        if self.mtdab:
            diff_lower = np.zeros(self.dim)
            diff_upper = np.zeros(self.dim)
            for cv in range(self.dim):
                diff_lower[cv] = self.cvs[cv].get_dev(self.values[cv], self.mtdmin[cv])
                diff_upper[cv] = self.cvs[cv].get_dev(self.values[cv], self.mtdmax[cv])
            self.history_k_lower.append(np.exp(-0.5*np.dot(diff_lower, diff_lower/self.widths2))/self.widths2)
            self.history_k_upper.append(np.exp(-0.5*np.dot(diff_upper, diff_upper/self.widths2))/self.widths2)

        if self.mtdfile is None:
            return

        line = ''
        line += '%12d' % self.nsteps
        for cv in range(self.dim):
            line += '%15.8f' % self.values[cv]
        for cv in range(self.dim):
            line += '%15.8f' % self.widths[cv]
        line += '%15.8f' % height
        line += '\n'

        self.mtdfile.write(line)
        self.mtdfile.flush()

    def get_pot_energy(self, atoms=None):
        return self.get_property('pot_energy', atoms)

    def get_pot_forces(self, atoms=None):
        return self.get_property('pot_forces', atoms)

    def get_mtd_energy(self, atoms=None):
        return self.get_property('mtd_energy', atoms)

    def get_mtd_forces(self, atoms=None):
        return self.get_property('mtd_forces', atoms)
