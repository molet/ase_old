import numpy as np
from numpy import linalg


class Distance:

    def __init__(self, atomi, atomj, label=None):
        self.atomi = atomi
        self.atomj = atomj
        self.label = label
        self.cv_type = 'Distance'

    def get_atom_indexes(self):

        return [self.atomi, self.atomj]

    def get_coord_indexes(self):

        return [3*self.atomi, 3*self.atomi+1, 3*self.atomi+2,
                3*self.atomj, 3*self.atomj+1, 3*self.atomj+2]

    def get_sum(self, v1, v2):

        summ = v1 + v2
        return summ

    def get_dev(self, v1, v2):

        diff = v1 - v2
        return diff

    def get_value(self, atoms):

        rij = rel_pos_pbc(atoms, self.atomi, self.atomj)
        v = linalg.norm(rij)

        return v

    def get_gradient(self, atoms):

        rij = rel_pos_pbc(atoms, self.atomi, self.atomj)
        dij = linalg.norm(rij)
        eij = rij/dij

        gx = np.zeros(6)

        gx[0:3] = eij
        gx[3:6] = -eij        

        return gx

    def get_hessian(self, atoms):

        rij = rel_pos_pbc(atoms, self.atomi, self.atomj)
        dij = linalg.norm(rij)
        eij = rij/dij

        Pij = np.tensordot(eij,eij,axes=0)
        Qij = np.eye(3)-Pij

        Hr = Qij/dij

        Hx = np.zeros((6,6))

        Hx[0:3,0:3] = Hr
        Hx[0:3,3:6] = -Hr
        Hx[3:6,0:3] = -Hr
        Hx[3:6,3:6] = Hr

        return Hx

Dis = Distance


class Angle:

    def __init__(self, atomi, atomj, atomk, label=None):
        self.atomi = atomi
        self.atomj = atomj
        self.atomk = atomk
        self.label = label
        self.cv_type = 'Angle'

    def get_atom_indexes(self):

        return [self.atomi, self.atomj, self.atomk]

    def get_coord_indexes(self):

        return [3*self.atomi, 3*self.atomi+1, 3*self.atomi+2,
                3*self.atomj, 3*self.atomj+1, 3*self.atomj+2,
                3*self.atomk, 3*self.atomk+1, 3*self.atomk+2]

    def get_sum(self, v1, v2):

        per = np.pi
        summ = v1 + v2
        return summ - round(summ / per) * per

    def get_dev(self, v1, v2):

        per = np.pi
        diff = v1 - v2
        return diff - round(diff / per) * per

    def get_value(self, atoms):

        rij = rel_pos_pbc(atoms, self.atomi, self.atomj)
        dij = linalg.norm(rij)
        eij = rij/dij
        rkj = rel_pos_pbc(atoms, self.atomk, self.atomj)
        dkj = linalg.norm(rkj)
        ekj = rkj/dkj
        eijekj = np.dot(eij, ekj)
        if np.abs(eijekj) > 1.0:
            eijekj = np.sign(eijekj)

        v = np.arccos(eijekj)

        return v

    def get_gradient(self, atoms, eps=1.0e-3):

        rij = rel_pos_pbc(atoms, self.atomi, self.atomj)
        dij = linalg.norm(rij)
        eij = rij/dij
        rkj = rel_pos_pbc(atoms, self.atomk, self.atomj)
        dkj = linalg.norm(rkj)
        ekj = rkj/dkj
        eijekj = np.dot(eij, ekj)
        if np.abs(eijekj) > 1.0:
            eijekj = np.sign(eijekj)

        a = np.arccos(eijekj)
        sina = np.sin(a)

        gr = np.zeros(6)

        if np.abs(sina) > eps:
            Pij = np.tensordot(eij,eij,axes=0)
            Qij = np.eye(3)-Pij
            Pkj = np.tensordot(ekj,ekj,axes=0)
            Qkj = np.eye(3)-Pkj

            gr[0:3] = -np.dot(Qij,ekj)/sina/dij
            gr[3:6] = -np.dot(Qkj,eij)/sina/dkj

        gx = np.zeros(9)

        gx[0:3] = gr[0:3]
        gx[3:6] = -gr[0:3]-gr[3:6]
        gx[6:9] = gr[3:6]

        return gx

    def get_hessian(self, atoms, eps=1.0e-3):

        rij = rel_pos_pbc(atoms, self.atomi, self.atomj)
        dij = linalg.norm(rij)
        dij2 = dij*dij
        eij = rij/dij
        rkj = rel_pos_pbc(atoms, self.atomk, self.atomj)
        dkj = linalg.norm(rkj)
        dkj2 = dkj*dkj
        ekj = rkj/dkj
        dijdkj = dij*dkj
        eijekj = np.dot(eij, ekj)
        if np.abs(eijekj) > 1.0:
            eijekj = np.sign(eijekj)

        a = np.arccos(eijekj)
        sina = np.sin(a)

        Hr = np.zeros((6,6))

        if np.abs(sina) > eps:
            cosa = np.cos(a)
            ctga = cosa/sina

            Pij = np.tensordot(eij,eij,axes=0)
            Qij = np.eye(3)-Pij
            Pkj = np.tensordot(ekj,ekj,axes=0)
            Qkj = np.eye(3)-Pkj
            Pik = np.tensordot(eij,ekj,axes=0)
            Pki = np.tensordot(ekj,eij,axes=0)
            P = np.eye(3)*eijekj

            QijPkjQij = np.dot(Qij, np.dot(Pkj, Qij))
            QijPkiQkj = np.dot(Qij, np.dot(Pki, Qkj))
            QkjPijQkj = np.dot(Qkj, np.dot(Pij, Qkj))

            Hr[0:3,0:3] = ( (-ctga*QijPkjQij/sina+np.dot(Qij, Pki)
                           -np.dot(Pij, Pki)*2.0+(Pik+P))/sina/dij2 )
            Hr[0:3,3:6] = ( (-ctga*QijPkiQkj/sina
                           -np.dot(Qij, Qkj))/sina/dijdkj )
            Hr[3:6,0:3] = Hr[0:3,3:6].T
            Hr[3:6,3:6] = ( (-ctga*QkjPijQkj/sina+np.dot(Qkj, Pik)
                           -np.dot(Pkj, Pik)*2.0+(Pki+P))/sina/dkj2 )

        Hx = np.zeros((9,9)) 

        Hx[0:3,0:3] = Hr[0:3,0:3]
        Hx[0:3,3:6] = -Hr[0:3,0:3]-Hr[0:3,3:6]
        Hx[0:3,6:9] = Hr[0:3,3:6]
        Hx[6:9,0:3] = Hr[3:6,0:3]
        Hx[6:9,3:6] = -Hr[3:6,0:3]-Hr[3:6,3:6]
        Hx[6:9,6:9] = Hr[3:6,3:6]
        Hx[3:6,0:3] = Hx[0:3,3:6]
        Hx[3:6,3:6] = -Hx[0:3,3:6]-Hx[6:9,3:6]
        Hx[3:6,6:9] = Hx[6:9,3:6]

        return Hx

Ang = Angle


class Dihedral:

    def __init__(self, atomi, atomj, atomk, atoml, label=None):
        self.atomi = atomi
        self.atomj = atomj
        self.atomk = atomk
        self.atoml = atoml
        self.label = label
        self.cv_type = 'Dihedral'

    def get_atom_indexes(self):

        return [self.atomi, self.atomj, self.atomk, self.atoml]

    def get_coord_indexes(self):

        return [3*self.atomi, 3*self.atomi+1, 3*self.atomi+2,
                3*self.atomj, 3*self.atomj+1, 3*self.atomj+2,
                3*self.atomk, 3*self.atomk+1, 3*self.atomk+2,
                3*self.atoml, 3*self.atoml+1, 3*self.atoml+2]

    def get_sum(self, v1, v2):

        per = 2.0 * np.pi
        summ = v1 + v2
        return summ - round(summ / per) * per

    def get_dev(self, v1, v2):

        per = 2.0 * np.pi
        diff = v1 - v2
        return diff - round(diff / per) * per

    def get_value(self, atoms):

        rij = rel_pos_pbc(atoms, self.atomi, self.atomj)
        rkj = rel_pos_pbc(atoms, self.atomk, self.atomj)
        rkl = rel_pos_pbc(atoms, self.atomk, self.atoml)

        rmj = np.cross(rij, rkj)
        dmj = np.linalg.norm(rmj)
        emj = rmj/dmj
        rnk = np.cross(rkj, rkl)
        dnk = np.linalg.norm(rnk)
        enk = rnk/dnk
        emjenk = np.dot(emj, enk)

        if np.abs(emjenk) > 1.0:
            emjenk = np.sign(emjenk)

        v = np.sign(np.dot(rkj, np.cross(rmj, rnk)))*np.arccos(emjenk)

        return v

    def get_gradient(self, atoms):

        rij = rel_pos_pbc(atoms, self.atomi, self.atomj)
        rkj = rel_pos_pbc(atoms, self.atomk, self.atomj)
        dkj = np.linalg.norm(rkj)
        dkj2 = dkj*dkj
        rkl = rel_pos_pbc(atoms, self.atomk, self.atoml)

        rijrkj = np.dot(rij, rkj)
        rkjrkl = np.dot(rkj, rkl)

        rmj = np.cross(rij, rkj)
        dmj2 = np.dot(rmj, rmj)
        rnk = np.cross(rkj, rkl)
        dnk2 = np.dot(rnk, rnk) 

        dddri = dkj/dmj2*rmj
        dddrl = -dkj/dnk2*rnk

        gx = np.zeros(12)

        gx[0:3] = dddri
        gx[3:6] = (rijrkj/dkj2-1.0)*dddri-rkjrkl/dkj2*dddrl
        gx[6:9] = (rkjrkl/dkj2-1.0)*dddrl-rijrkj/dkj2*dddri
        gx[9:12] = dddrl

        return gx

    def get_hessian(self, atoms):

        rij = rel_pos_pbc(atoms, self.atomi, self.atomj)
        rkj = rel_pos_pbc(atoms, self.atomk, self.atomj)
        dkj = np.linalg.norm(rkj)
        dkj2 = dkj*dkj
        dkj4 = dkj2*dkj2
        ekj = rkj/dkj
        rkl = rel_pos_pbc(atoms, self.atomk, self.atoml)

        rijrkj = np.dot(rij, rkj)
        rkjrkl = np.dot(rkj, rkl)

        rmj = np.cross(rij, rkj)
        dmj2 = np.dot(rmj, rmj)
        dmj4 = dmj2*dmj2
        rnk = np.cross(rkj, rkl)
        dnk2 = np.dot(rnk, rnk)
        dnk4 = dnk2*dnk2

        dddri = dkj/dmj2*rmj
        dddrl = -dkj/dnk2*rnk

        ddkjdrj = -ekj
        ddkjdrk = ekj

        drmjdri = np.zeros((3,3))
        drmjdri[0,1] = -rkj[2]
        drmjdri[0,2] = rkj[1]
        drmjdri[1,0] = rkj[2]
        drmjdri[1,2] = -rkj[0]
        drmjdri[2,0] = -rkj[1]
        drmjdri[2,1] = rkj[0]
        drmjdrj = np.zeros((3,3))
        drmjdrj[0,1] = rkj[2]-rij[2]
        drmjdrj[0,2] = rij[1]-rkj[1]
        drmjdrj[1,0] = rij[2]-rkj[2]
        drmjdrj[1,2] = rkj[0]-rij[0]
        drmjdrj[2,0] = rkj[1]-rij[1]
        drmjdrj[2,1] = rij[0]-rkj[0]
        drmjdrk = np.zeros((3,3))
        drmjdrk[0,1] = rij[2]
        drmjdrk[0,2] = -rij[1]
        drmjdrk[1,0] = -rij[2]
        drmjdrk[1,2] = rij[0]
        drmjdrk[2,0] = rij[1]
        drmjdrk[2,1] = -rij[0]

        drnkdrj = np.zeros((3,3))
        drnkdrj[0,1] = rkl[2]
        drnkdrj[0,2] = -rkl[1]
        drnkdrj[1,0] = -rkl[2]
        drnkdrj[1,2] = rkl[0]
        drnkdrj[2,0] = rkl[1]
        drnkdrj[2,1] = -rkl[0]
        drnkdrk = np.zeros((3,3))
        drnkdrk[0,1] = -rkl[2]+rkj[2]
        drnkdrk[0,2] = -rkj[1]+rkl[1]
        drnkdrk[1,0] = -rkj[2]+rkl[2]
        drnkdrk[1,2] = -rkl[0]+rkj[0]
        drnkdrk[2,0] = -rkl[1]+rkj[1]
        drnkdrk[2,1] = -rkj[0]+rkl[0]
        drnkdrl = np.zeros((3,3))
        drnkdrl[0,1] = -rkj[2]
        drnkdrl[0,2] = rkj[1]
        drnkdrl[1,0] = rkj[2]
        drnkdrl[1,2] = -rkj[0]
        drnkdrl[2,0] = -rkj[1]
        drnkdrl[2,1] = rkj[0]

        drijrkjdrj = -rkj-rij
        drijrkjdrk = rij

        drkjrkldrj = -rkl
        drkjrkldrk = rkl+rkj

        Hx = np.zeros((12,12))

        Hx[0:3,0:3] = np.tensordot(-2.0*dkj*np.dot(drmjdri,rmj)
                                   /dmj4,rmj,axes=0)+dkj/dmj2*drmjdri
        Hx[3:6,0:3] = np.tensordot((dmj2*ddkjdrj-2.0*dkj*np.dot(drmjdrj,rmj))
                                   /dmj4,rmj,axes=0)+dkj/dmj2*drmjdrj
        Hx[0:3,3:6] = Hx[3:6,0:3].T
        Hx[6:9,0:3] = np.tensordot((dmj2*ddkjdrk-2.0*dkj*np.dot(drmjdrk,rmj))
                                   /dmj4,rmj,axes=0)+dkj/dmj2*drmjdrk
        Hx[0:3,6:9] = Hx[6:9,0:3].T
        Hx[9:12,9:12] = np.tensordot(2.0*dkj*np.dot(drnkdrl,rnk)
                                     /dnk4,rnk,axes=0)-dkj/dnk2*drnkdrl
        Hx[3:6,9:12] = np.tensordot((-dnk2*ddkjdrj+2.0*dkj*np.dot(drnkdrj,rnk))
                                     /dnk4,rnk,axes=0)-dkj/dnk2*drnkdrj
        Hx[9:12,3:6] = Hx[3:6,9:12].T
        Hx[6:9,9:12] = np.tensordot((-dnk2*ddkjdrk+2.0*dkj*np.dot(drnkdrk,rnk))
                                    /dnk4,rnk,axes=0)-dkj/dnk2*drnkdrk
        Hx[9:12,6:9] = Hx[6:9,9:12].T
        Hx[3:6,3:6] = ( np.tensordot((dkj2*drijrkjdrj-2.0*rijrkj*dkj*ddkjdrj)
                                     /dkj4,dddri,axes=0)
                       +(rijrkj/dkj2-1.0)*Hx[3:6,0:3]
                       -np.tensordot((dkj2*drkjrkldrj-2.0*rkjrkl*dkj*ddkjdrj)
                                     /dkj4,dddrl,axes=0)
                       -(rkjrkl/dkj2)*Hx[3:6,9:12] )
        Hx[6:9,3:6] = ( np.tensordot((dkj2*drijrkjdrk-2.0*rijrkj*dkj*ddkjdrk)
                                     /dkj4,dddri,axes=0)
                       +(rijrkj/dkj2-1.0)*Hx[6:9,0:3]
                       -np.tensordot((dkj2*drkjrkldrk-2.0*rkjrkl*dkj*ddkjdrk)
                                     /dkj4,dddrl,axes=0)
                       -(rkjrkl/dkj2)*Hx[6:9,9:12] )
        Hx[3:6,6:9] = Hx[6:9,3:6].T
        Hx[6:9,6:9] = ( np.tensordot((dkj2*drkjrkldrk-2.0*rkjrkl*dkj*ddkjdrk)
                                     /dkj4,dddrl,axes=0)
                       +(rkjrkl/dkj2-1.0)*Hx[6:9,9:12]
                       -np.tensordot((dkj2*drijrkjdrk-2.0*rijrkj*dkj*ddkjdrk)
                                     /dkj4,dddri,axes=0)
                       -(rijrkj/dkj2)*Hx[6:9,0:3] )

        return Hx

Dih = Dihedral


def rel_pos_pbc(atoms, i, j):
    """
    Return difference between two atomic positions, 
    correcting for jumps across PBC
    """

    if np.all(~atoms.get_pbc()):
        return atoms.get_positions()[i,:]-atoms.get_positions()[j,:]
    if not np.all(atoms.get_pbc()):
        return atoms.get_distance(j, i, mic=True, vector=True)
    d = atoms.get_positions()[i,:]-atoms.get_positions()[j,:]
    c = atoms.get_cell().T
    f = np.floor(linalg.solve(c, d.T) + 0.5)
    d -= np.dot(c, f).T
    return d


def translational_vectors(atoms, mass_weighted=False):
    """
    Return normalised translational vectors
    """

    Tr = np.zeros((3*len(atoms),3))

    if mass_weighted:
        masses = atoms.get_masses()
    else:
        masses = np.ones(len(atoms))
    masses_sqrt = np.sqrt(masses)

    k=0
    for i in range(len(atoms)):
        for j in range(3):
           Tr[k,j] = masses_sqrt[i]
           k+=1

    for i in range(3):
        norm = np.sqrt(np.dot(Tr[:,i], Tr[:,i]))
        Tr[:,i] /= norm

    return Tr


def rotational_vectors(atoms, mass_weighted=False):
    """
    Return normalised rotational vectors
    """

    Rot = np.zeros((3*len(atoms),3))

    threshold = np.finfo(float).eps*10000.0

    if mass_weighted:
        masses = atoms.get_masses()
    else:
        masses = np.ones(len(atoms))
    masses_sqrt = np.sqrt(masses)

    com = np.zeros(3)
    for i in range(len(atoms)):
        com += masses[i] * atoms.get_positions()[i,:]
    com /= np.sum(masses)

    it = np.zeros((3,3))
    for i in range(len(atoms)):
        rpos = atoms.get_positions()[i,:] - com
        it[0,0] += masses[i] * (rpos[1]**2 + rpos[2]**2)
        it[1,1] += masses[i] * (rpos[0]**2 + rpos[2]**2)
        it[2,2] += masses[i] * (rpos[0]**2 + rpos[1]**2)
        it[0,1] -= masses[i] * (rpos[0]*rpos[1])
        it[0,2] -= masses[i] * (rpos[0]*rpos[2])
        it[1,2] -= masses[i] * (rpos[1]*rpos[2])
    it[1,0] = it[0,1]
    it[2,0] = it[0,2]
    it[2,1] = it[1,2]
    d, dit = linalg.eigh(it)

    for i in range(len(atoms)):
        rpos = atoms.get_positions()[i,:] - com
        cp = np.dot(np.transpose(dit), rpos)
        Rot[i*3,0] = masses_sqrt[i] * (cp[1]*dit[0,2]-cp[2]*dit[0,1])
        Rot[i*3+1,0] = masses_sqrt[i] * (cp[1]*dit[1,2]-cp[2]*dit[1,1])
        Rot[i*3+2,0] = masses_sqrt[i] * (cp[1]*dit[2,2]-cp[2]*dit[2,1])

        Rot[i*3,1] = masses_sqrt[i] * (cp[2]*dit[0,0]-cp[0]*dit[0,2])
        Rot[i*3+1,1] = masses_sqrt[i] * (cp[2]*dit[1,0]-cp[0]*dit[1,2])
        Rot[i*3+2,1] = masses_sqrt[i] * (cp[2]*dit[2,0]-cp[0]*dit[2,2])

        Rot[i*3,2] = masses_sqrt[i] * (cp[0]*dit[0,1]-cp[1]*dit[0,0])
        Rot[i*3+1,2] = masses_sqrt[i] * (cp[0]*dit[1,1]-cp[1]*dit[1,0])
        Rot[i*3+2,2] = masses_sqrt[i] * (cp[0]*dit[2,1]-cp[1]*dit[2,0])

    ndof = 3
    for i in range(3):
        norm = np.sqrt(np.dot(Rot[:,i], Rot[:,i]))
        if norm <= threshold:
            ndof -= 1
            continue
        Rot[:,i] /= norm
        if i < 2:
            for j in range(i+1):
                Rot[:,i+1] = Rot[:,i+1] - np.dot(Rot[:,i+1],Rot[:,j]) * Rot[:,j]

    return Rot[:,0:ndof]


def remove_tr_rot_vector(atoms, vecin, mass_weighted=False):

    Tr = translational_vectors(atoms, mass_weighted)
    Rot = rotational_vectors(atoms, mass_weighted)

    vecout = vecin

    for i in range(np.shape(Tr)[1]):
        norm = np.dot(vecout, Tr[:,i])
        vecout -= norm * Tr[:,i]

    for i in range(np.shape(Rot)[1]):
        norm = np.dot(vecout, Rot[:,i])
        vecout -= norm * Rot[:,i]

    return vecout

