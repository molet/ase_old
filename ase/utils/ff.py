import numpy as np
from numpy import linalg
from ase import units 
from ase.utils.cv import Distance, Angle, Dihedral
from scipy import sparse

class Potential:

    def get_potential_value(self, atoms):
        raise NotImplementedError("get_potential_value must be implemented!")

    def get_potential_derivative(self, atoms):
        raise NotImplementedError("get_potential_derivative must be implemented!")

    def get_potential_curvature(self, atoms):
        raise NotImplementedError("get_potential_curvature must be implemented!")

    def get_potential_gradient(self, atoms):

        pd = self.get_potential_derivative(atoms)
        gx = self.get_gradient(atoms)

        pgx = pd*gx

        return pgx 

    def get_potential_hessian(self, atoms, hessian="normal"):

        pc = self.get_potential_curvature(atoms)
        gx = self.get_gradient(atoms)

        if hessian == "reduced":
            pHx = np.abs(pc)*np.tensordot(gx,gx,axes=0)
        else:
            pd = self.get_potential_derivative(atoms)
            Hx = self.get_hessian(atoms)
            pHx = pc*np.tensordot(gx,gx,axes=0) + pd*Hx

        if hessian == "spectral":
            eigvals, eigvecs = linalg.eigh(pHx)
            D = np.diag(np.abs(eigvals))
            U = eigvecs
            pHx = np.dot(U,np.dot(D,np.transpose(U)))

        return pHx

class Morse_Bond(Distance, Potential):

    def __init__(self, atomi, atomj, 
                 D=None, alpha=None, r0=None):
        Distance.__init__(self, atomi, atomj)
        self.D = D
        self.alpha = alpha
        self.r0 = r0
        self.pot_type = 'morse_bond'

    def get_potential_value(self, atoms):

        v = self.get_value(atoms)
        exp = np.exp(-self.alpha*(v-self.r0))

        pv = self.D*(1.0-exp)**2

        return pv

    def get_potential_derivative(self, atoms):

        v = self.get_value(atoms)
        exp = np.exp(-self.alpha*(v-self.r0))

        pd = 2.0*self.D*self.alpha*exp*(1.0-exp)

        return pd

    def get_potential_curvature(self, atoms):

        v = self.get_value(atoms)
        exp = np.exp(-self.alpha*(v-self.r0))

        pc = 2.0*self.D*self.alpha**2*exp*(2.0*exp-1.0)

        return pc

    def get_eta_value(self, atoms):

        v = self.get_value(atoms)
        exp = np.exp(-self.alpha*(v-self.r0))

        ev = 1.0 - (1.0 - exp)**2

        return ev

    def get_eta_derivative(self, atoms):

        v = self.get_value(atoms)
        exp = np.exp(-self.alpha*(v-self.r0))

        ed = -2.0*self.alpha*exp*(1.0-exp)

        return ed

    def get_eta_curvature(self, atoms):

        v = self.get_value(atoms)
        exp = np.exp(-self.alpha*(v-self.r0))

        ec = 2.0*self.alpha**2*exp*(1.0-2.0*exp)

        return ec

    def get_eta_gradient(self, atoms):

        ed = self.get_eta_derivative(atoms)
        gx = self.get_gradient(atoms)

        egx = ed*gx

        return egx

    def get_eta_hessian(self, atoms, hessian="normal"):

        ec = self.get_eta_curvature(atoms)
        gx = self.get_gradient(atoms)

        if hessian == "reduced":
            eHx = np.abs(ec)*np.tensordot(gx,gx,axes=0)
        else:
            ed = self.get_eta_derivative(atoms)
            Hx = self.get_hessian(atoms)
            eHx = ec*np.tensordot(gx,gx,axes=0) + ed*Hx

        if hessian == "spectral":
            eigvals, eigvecs = linalg.eigh(eHx)
            D = np.diag(np.abs(eigvals))
            U = eigvecs
            eHx = np.dot(U,np.dot(D,np.transpose(U)))

        return eHx


class Harmonic_Bond(Distance, Potential):

    def __init__(self, atomi, atomj, 
                 k=None, b0=None):
        Distance.__init__(self, atomi, atomj)
        self.k = k
        self.b0 = b0
        self.pot_type = "harmonic_bond"

    def get_potential_value(self, atoms):

        v = self.get_value(atoms)

        pv = 0.5*self.k*(v-self.b0)**2

        return pv

    def get_potential_derivative(self, atoms):

        v = self.get_value(atoms)

        pd = self.k*(v-self.b0)

        return pd

    def get_potential_curvature(self, atoms):

        pc = self.k

        return pc


class UFF_Bond(Distance, Potential):

    def __init__(self, atomi, atomj,
                 k=None, b0=None):
        Distance.__init__(self, atomi, atomj)
        self.k = k
        self.b0 = b0
        self.pot_type = "uff_bond"

    def get_potential_value(self, atoms):

        v = self.get_value(atoms)

        pv = self.k*(v-self.b0)**2

        return pv

    def get_potential_derivative(self, atoms):

        v = self.get_value(atoms)

        pd = 2.0*self.k*(v-self.b0)

        return pd

    def get_potential_curvature(self, atoms):

        pc = 2.0*self.k

        return pc


class Model_Bond(Distance, Potential):

    def __init__(self, atomi, atomj,
                 k=None, alphaij=None, rrefij=None):
        Distance.__init__(self, atomi, atomj)
        self.k = k
        self.alphaij = alphaij
        self.rrefij2 = rrefij**2
        self.rij = Distance(atomi, atomj)
        self.pot_type = "model_bond"

    def get_potential_value(self, atoms):

        pv = 0.0 

        return pv

    def get_potential_derivative(self, atoms):

        pd = 0.0

        return pd

    def get_potential_curvature(self, atoms):

        rhoij = np.exp(self.alphaij*(self.rrefij2-self.rij.get_value(atoms)**2))

        pc = self.k*rhoij

        return pc


class Harmonic_Angle(Angle, Potential):

    def __init__(self, atomi, atomj, atomk, 
                 k=None, a0=None):
        Angle.__init__(self, atomi, atomj, atomk)
        self.k = k
        self.a0 = a0
        self.pot_type = "harmonic_angle"

    def get_potential_value(self, atoms):

        v = self.get_value(atoms)

        pv = 0.5*self.k*self.get_dev(v, self.a0)**2

        return pv

    def get_potential_derivative(self, atoms):

        v = self.get_value(atoms)

        pd = self.k*self.get_dev(v, self.a0)

        return pd

    def get_potential_curvature(self, atoms):

        pc = self.k

        return pc


class Harmonic_CosAngle(Angle, Potential):

    def __init__(self, atomi, atomj, atomk,
                 k=None, a0=None):
        Angle.__init__(self, atomi, atomj, atomk)
        self.k = k
        self.a0 = a0
        self.pot_type = "harmonic_cosangle"

    def get_potential_value(self, atoms):

        v = self.get_value(atoms)

        pv = 0.5*self.k*(np.cos(v)-np.cos(self.a0))**2

        return pv

    def get_potential_derivative(self, atoms):

        v = self.get_value(atoms)

        pd = -self.k*(np.cos(v)-np.cos(self.a0))*np.sin(v)

        return pd

    def get_potential_curvature(self, atoms):

        v = self.get_value(atoms)

        pc = self.k*(1.0-2.0*np.cos(v)**2+np.cos(v)*np.cos(self.a0))

        return pc


class UFF_Angle(Angle, Potential):

    def __init__(self, atomi, atomj, atomk,
                 k=None, a0=None, coord=None, c0=None, c1=None, c2=None, exp=False):
        Angle.__init__(self, atomi, atomj, atomk)
        self.k = k
        self.a0 = a0
        self.coord = coord
        if coord == 2:
            self.n = 3.0
        elif coord == 4:
            self.n = 4.0
        elif coord == 6:
            self.n = 4.0
        else:
            self.n = None
        self.c0 = c0
        self.c1 = c1
        self.c2 = c2
        self.exp = exp
        self.pot_type = "uff_angle"

    def get_potential_value(self, atoms):

        v = self.get_value(atoms)

        if self.coord == 1:
            pv = self.k*(1.0+np.cos(v))
        elif self.coord == 2:
            pv = self.k*(1.0-np.cos(self.n*v))
        elif self.coord == 4:
            pv = self.k*(1.0-np.cos(self.n*v))
        elif self.coord == 6:
            pv = self.k*(1.0-np.cos(self.n*v))
        elif self.coord == 7:
            cosv = np.cos(v)
            pv = self.k*self.c1*(cosv-0.30901699)**2*(cosv+0.80901699)**2
        else:
            cosv = np.cos(v)
            pv = self.k*(self.c0+self.c1*cosv+self.c2*(2.0*cosv*cosv-1.0))

        if self.exp:
            pv += 0.01*np.exp(-20.0*(v-self.a0+0.25))

        return pv

    def get_potential_derivative(self, atoms):

        v = self.get_value(atoms)

        if self.coord == 1:
            pd = -self.k*np.sin(v)
        elif self.coord == 2:
            pd = self.k*self.n*np.sin(self.n*v)
        elif self.coord == 4:
            pd = self.k*self.n*np.sin(self.n*v)
        elif self.coord == 6:
            pd = self.k*self.n*np.sin(self.n*v)
        elif self.coord == 7:
            cosv = np.cos(v)
            sinv = np.sin(v)
            pd = -2.0*self.k*self.c1*sinv*(cosv-0.30901699)*(cosv+0.80901699)*(2.0*cosv+0.5)
        else:
            cosv = np.cos(v)
            sinv = np.sin(v)
            pd = -self.k*sinv*(self.c1+4.0*self.c2*cosv)

        if self.exp:
            pd += -0.2*np.exp(-20.0*(v-self.a0+0.25))

        return pd

    def get_potential_curvature(self, atoms):

        v = self.get_value(atoms)

        if self.coord == 1:
            pc = -self.k*np.cos(v)
        elif self.coord == 2: 
            pc = self.k*self.n*self.n*np.cos(self.n*v)
        elif self.coord == 4:
            pc = self.k*self.n*self.n*np.cos(self.n*v)
        elif self.coord == 6:
            pc = self.k*self.n*self.n*np.cos(self.n*v)
        elif self.coord == 7:
            cosv = np.cos(v)
            sinv = np.sin(v)
            pc = -2.0*self.k*self.c1*(cosv*(cosv-0.30901699)*(cosv+0.80901699)*(2.0*cosv+0.5)
                                     -sinv*sinv*((2.0*cosv+0.5)**2+(cosv-0.30901699)*(cosv+0.80901699)))
        else:
            cosv = np.cos(v)
            pc = -self.k*(self.c1*cosv+8.0*self.c2*cosv*cosv-4.0*self.c2)

        if self.exp:
            pc += 4.0*np.exp(-20.0*(v-self.a0+0.25))

        return pc


class Model_Angle(Angle, Potential):

    def __init__(self, atomi, atomj, atomk,
                 k=None, alphaij=None, alphajk=None, rrefij=None, rrefjk=None):
        Angle.__init__(self, atomi, atomj, atomk)
        self.k = k
        self.alphaij = alphaij
        self.alphajk = alphajk
        self.rrefij2 = rrefij**2
        self.rrefjk2 = rrefjk**2
        self.rij = Distance(atomi, atomj)
        self.rjk = Distance(atomj, atomk)
        self.pot_type = "model_angle"

    def get_potential_value(self, atoms):

        pv = 0.0

        return pv

    def get_potential_derivative(self, atoms):

        pd = 0.0

        return pd

    def get_potential_curvature(self, atoms):

        rhoij = np.exp(self.alphaij*(self.rrefij2-self.rij.get_value(atoms)**2))
        rhojk = np.exp(self.alphajk*(self.rrefjk2-self.rjk.get_value(atoms)**2))

        pc = self.k*rhoij*rhojk

        return pc


class Harmonic_Dihedral(Dihedral, Potential):

    def __init__(self, atomi, atomj, atomk, atoml,
                 k=None, d0=None):
        Dihedral.__init__(self, atomi, atomj, atomk, atoml)
        self.k = k
        self.d0 = d0
        self.pot_type = "harmonic_dihedral"

    def get_potential_value(self, atoms):

        v = self.get_value(atoms)

        pv = 0.5*self.k*self.get_dev(v, self.d0)**2

        return pv

    def get_potential_derivative(self, atoms):

        v = self.get_value(atoms)

        pd = self.k*self.get_dev(v, self.d0)

        return pd

    def get_potential_curvature(self, atoms):

        pc = self.k

        return pc


class Fourier_Dihedral(Dihedral, Potential):

    def __init__(self, atomi, atomj, atomk, atoml, 
                 k=None, d0=None, n=None):
        Dihedral.__init__(self, atomi, atomj, atomk, atoml)
        self.k = k
        self.d0 = d0
        self.n = n
        self.pot_type = "fourier_dihedral"

    def get_potential_value(self, atoms):

        v = self.get_value(atoms)

        pv = self.k*(1.0+np.cos(self.n*v-self.d0))

        return pv

    def get_potential_derivative(self, atoms):

        v = self.get_value(atoms)

        pd = -self.k*self.n*np.sin(self.n*v-self.d0)

        return pd

    def get_potential_curvature(self, atoms):

        v = self.get_value(atoms)

        pc = -self.k*self.n**2*np.cos(self.n*v-self.d0)

        return pc


class Cosine_Dihedral(Dihedral, Potential):

    def __init__(self, atomi, atomj, atomk, atoml,
                 k=None):
        Dihedral.__init__(self, atomi, atomj, atomk, atoml)
        self.k = k
        self.pot_type = "cosine_dihedral"

    def get_potential_value(self, atoms):

        v = self.get_value(atoms)

        pv = 0.5*self.k*(1.0-np.cos(2.0*v))

        return pv

    def get_potential_derivative(self, atoms):

        v = self.get_value(atoms)

        pd = self.k*np.sin(2.0*v)

        return pd

    def get_potential_curvature(self, atoms):

        v = self.get_value(atoms)

        pc = 2.0*self.k*np.cos(2.0*v)

        return pc


class UFF_Dihedral(Dihedral, Potential):

    def __init__(self, atomi, atomj, atomk, atoml,
                 k=None, d0=None, n=None):
        Dihedral.__init__(self, atomi, atomj, atomk, atoml)
        self.k = k
        self.d0 = d0
        self.n = n
        self.pot_type = "uff_dihedral"

    def get_potential_value(self, atoms):

        v = self.get_value(atoms)

        pv = self.k*(1.0-np.cos(self.n*self.d0)*np.cos(self.n*v))

        return pv

    def get_potential_derivative(self, atoms):

        v = self.get_value(atoms)

        pd = self.k*self.n*np.cos(self.n*self.d0)*np.sin(self.n*v)

        return pd

    def get_potential_curvature(self, atoms):

        v = self.get_value(atoms)

        pc = self.k*self.n**2*np.cos(self.n*self.d0)*np.cos(self.n*v)

        return pc


class Model_Dihedral(Dihedral, Potential):

    def __init__(self, atomi, atomj, atomk, atoml,
                 k=None, alphaij=None, alphajk=None, alphakl=None,
                 rrefij=None, rrefjk=None, rrefkl=None):
        Dihedral.__init__(self, atomi, atomj, atomk, atoml)
        self.k = k
        self.alphaij = alphaij
        self.alphajk = alphajk
        self.alphakl = alphakl
        self.rrefij2 = rrefij**2
        self.rrefjk2 = rrefjk**2
        self.rrefkl2 = rrefkl**2
        self.rij = Distance(atomi, atomj)
        self.rjk = Distance(atomj, atomk)
        self.rkl = Distance(atomk, atoml)
        self.pot_type = "model_dihedral"

    def get_potential_value(self, atoms):

        pv = 0.0

        return pv

    def get_potential_derivative(self, atoms):

        pd = 0.0

        return pd

    def get_potential_curvature(self, atoms):

        rhoij = np.exp(self.alphaij*(self.rrefij2-self.rij.get_value(atoms)**2))
        rhojk = np.exp(self.alphajk*(self.rrefjk2-self.rjk.get_value(atoms)**2))
        rhokl = np.exp(self.alphakl*(self.rrefkl2-self.rkl.get_value(atoms)**2))

        pc = self.k*rhoij*rhojk*rhokl

        return pc


class VdW(Distance, Potential):

    def __init__(self, atomi, atomj, epsilonij=None, sigmaij=None, rminij=None,
                 Aij=None, Bij=None, epsiloni=None, epsilonj=None, 
                 sigmai=None, sigmaj=None, rmini=None, rminj=None, scale=1.0):
        Distance.__init__(self, atomi, atomj)
        if epsilonij is not None:
            if sigmaij is not None:
                self.Aij = scale * 4.0 * epsilonij * sigmaij**12
                self.Bij = scale * 4.0 * epsilonij * sigmaij**6
            elif rminij is not None:
                self.Aij = scale * epsilonij * rminij**12
                self.Bij = scale * 2.0 * epsilonij * rminij**6
            else:
                raise NotImplementedError("not implemented combination" 
                                          "of vdW parameters.")
        elif Aij is not None and Bij is not None:
            self.Aij = scale * Aij
            self.Bij = scale * Bij
        elif epsiloni is not None and epsilonj is not None:
            if sigmai is not None and sigmaj is not None:
                self.Aij = ( scale * 4.0 * np.sqrt(epsiloni * epsilonj)
                             * ((sigmai + sigmaj) / 2.0)**12 )
                self.Bij = ( scale * 2.0 * np.sqrt(epsiloni * epsilonj)
                             * ((sigmai + sigmaj) / 2.0)**6 )
            elif rmini is not None and rminj is not None:
                self.Aij = ( scale * np.sqrt(epsiloni * epsilonj)
                             * ((rmini + rminj) / 2.0)**12 )
                self.Bij = ( scale * 2.0 * np.sqrt(epsiloni * epsilonj) 
                             * ((rmini + rminj) / 2.0)**6 )
        else:
            raise NotImplementedError("not implemented combination"
                                      "of vdW parameters.")
        self.pot_type = "vdw"

    def get_potential_value(self, atoms):

        v = self.get_value(atoms)

        pv = self.Aij/v**12 - self.Bij/v**6

        return pv

    def get_potential_derivative(self, atoms):

        v = self.get_value(atoms)

        pd = -12.0*self.Aij/v**13+6.0*self.Bij/v**7

        return pd

    def get_potential_curvature(self, atoms):

        v = self.get_value(atoms)

        pc = 156.0*self.Aij/v**14-42.0*self.Bij/v**8

        return pc


class Coulomb(Distance, Potential):

    def __init__(self, atomi, atomj, chargeij=None, 
                 chargei=None, chargej=None, scale=1.0):
        Distance.__init__(self, atomi, atomj)
        if chargeij is not None:
            self.chargeij = ( scale * chargeij * 8.9875517873681764e9
                              * units.m * units.J / units.C / units.C )
        elif chargei is not None and chargej is not None:
            self.chargeij = ( scale * chargei * chargej * 8.9875517873681764e9
                            * units.m * units.J / units.C / units.C )
        else:
            raise NotImplementedError("not implemented combination"
                                      "of Coulomb parameters.")
        self.pot_type = "coulomb"

    def get_potential_value(self, atoms):

        v = self.get_value(atoms)

        pv = self.chargeij/v

        return pv

    def get_potential_derivative(self, atoms):

        v = self.get_value(atoms)

        pd = -self.chargeij/v**2

        return pd

    def get_potential_curvature(self, atoms):

        v = self.get_value(atoms)

        pc = 2.0*self.chargeij/v**3 

        return pc


def get_bonded_terms(atoms, cutoff=1.6):

    d = atoms.get_all_distances(mic=True)

    neighbor_list = [[] for _ in range(len(atoms))]

    for i in range(len(atoms)):
        for j in range(len(atoms)):
            if d[i,j] <= cutoff and i!=j:
                neighbor_list[i].append(j)

    bonds = []
    for i in range(len(atoms)):
        for j in neighbor_list[i]:
            if j>i:
                bonds.append([i,j])

    angles = []
    for bond in bonds:
        i = bond[0]
        j = bond[1]
        for k in neighbor_list[j]:
            if k>i:
                angles.append([i,j,k])
        for k in neighbor_list[i]:
            if k>j:
                angles.append([j,i,k])

    dihedrals = []
    for angle in angles:
        i = angle[0]
        j = angle[1]
        k = angle[2]
        for l in neighbor_list[k]:
            if l>i and l!=j:
                dihedrals.append([i,j,k,l])
        for l in neighbor_list[i]:
            if l>k and l!=j:
                dihedrals.append([l,i,j,k])

    torsions = []
    for angle in angles:
        i = angle[0]
        j = angle[1]
        k = angle[2]
        if len(neighbor_list[j])==3:
            for l in neighbor_list[j]:
                if l>i and l>k:
                    torsions.append([i,j,k,l])

    return bonds, angles, dihedrals, torsions


def model_terms_new(atoms, cutoff=1.6):

    alpha = np.array([[1.0000, 0.3949, 0.3949],
                      [0.3949, 0.2800, 0.2800],
                      [0.3949, 0.2800, 0.2800]]) / units.Bohr / units.Bohr
    rref = np.array([[1.35, 2.10, 2.53],
                     [2.10, 2.87, 3.40],
                     [2.53, 3.40, 3.40]]) * units.Bohr

    kbond = 0.45 * units.Hartree / units.Bohr / units.Bohr
    kangle = 0.15 * units.Hartree / units.Bohr / units.Bohr
    kdihedral = 0.005 * units.Hartree / units.Bohr / units.Bohr

    terms = []
    bonds = []
    angles = []
    dihedrals = [] 

    d = atoms.get_all_distances()

    for i in range(len(atoms)):
        for j in range(i+1, len(atoms)):
            dij = d[i,j]
            if dij <= cutoff:
                bonds.append([i, j])

    for m in range(len(bonds)):
        for n in range(m+1, len(bonds)):
            intersection = list(set(bonds[m]).intersection(bonds[n]))
            if len(intersection) == 1:
                xor = list(set(intersection)^set(bonds[n]))
                if bonds[m].index(intersection[0]) == 0:
                    angles.append(xor+bonds[m])
                else:
                    angles.append(bonds[m]+xor)

    for m in range(len(angles)):
        for n in range(len(bonds)):
            intersection = list(set(angles[m]).intersection(bonds[n]))
            if len(intersection) == 1:
                xor = list(set(intersection)^set(bonds[n]))
                if angles[m].index(intersection[0]) == 0:
                    dihedrals.append(xor+angles[m])
                elif angles[m].index(intersection[0]) == 2:
                    dihedrals.append(angles[m]+xor)

    for n in range(len(bonds)):
        i, j = bonds[n]
        rowi = row(atoms.get_atomic_numbers()[i])-1
        rowj = row(atoms.get_atomic_numbers()[j])-1
        terms.append(Model_Bond(i, j, kbond,
                     alphaij=alpha[rowi, rowj],
                     rrefij=rref[rowi, rowj]))

    for n in range(len(angles)):
        i, j, k = angles[n]
        rowi = row(atoms.get_atomic_numbers()[i])-1
        rowj = row(atoms.get_atomic_numbers()[j])-1
        rowk = row(atoms.get_atomic_numbers()[k])-1
        terms.append(Model_Angle(j, i, k, kangle,
                     alphaij=alpha[rowj, rowi], alphajk=alpha[rowi, rowk],
                     rrefij=rref[rowj, rowi], rrefjk=rref[rowi, rowk]))

    if len(atoms) <= 3:
        return terms 

    for n in range(len(dihedrals)):
        i, j, k, l = dihedrals[n]
        rowi = row(atoms.get_atomic_numbers()[i])-1
        rowj = row(atoms.get_atomic_numbers()[j])-1
        rowk = row(atoms.get_atomic_numbers()[k])-1
        rowl = row(atoms.get_atomic_numbers()[l])-1
        terms.append(Model_Dihedral(k, i, j, l, kdihedral,
                     alphaij=alpha[rowk, rowi], alphajk=alpha[rowi, rowj], alphakl=alpha[rowj, rowl],
                     rrefij=rref[rowk, rowi], rrefjk=rref[rowi, rowj], rrefkl=rref[rowj, rowl]))

    return terms


def model_terms(atoms, cutoff=10.0):

    alpha = np.array([[1.0000, 0.3949, 0.3949], 
                      [0.3949, 0.2800, 0.2800], 
                      [0.3949, 0.2800, 0.2800]]) / units.Bohr / units.Bohr
    rref = np.array([[1.35, 2.10, 2.53], 
                     [2.10, 2.87, 3.40], 
                     [2.53, 3.40, 3.40]]) * units.Bohr

    kbond = 0.45 * units.Hartree / units.Bohr / units.Bohr
    kangle = 0.15 * units.Hartree / units.Bohr / units.Bohr
    kdihedral = 0.005 * units.Hartree / units.Bohr / units.Bohr

    terms = []

    d = atoms.get_all_distances()

    for i in range(len(atoms)):
        rowi = row(atoms.get_atomic_numbers()[i])-1
        for j in range(i+1, len(atoms)):
            dij = d[i,j]
            rowj = row(atoms.get_atomic_numbers()[j])-1
            if dij <= cutoff:
                terms.append(Model_Bond(i, j, kbond,
                             alphaij=alpha[rowi, rowj], 
                             rrefij=rref[rowi, rowj]))
            for k in range(j+1, len(atoms)):
                 dik = d[i,k]
                 djk = d[j,k]
                 rowk = row(atoms.get_atomic_numbers()[k])-1
                 if dij <= cutoff:
                     if dik <= cutoff:
                         terms.append(Model_Angle(j, i, k, kangle,
                                      alphaij=alpha[rowj, rowi], alphajk=alpha[rowi, rowk],
                                      rrefij=rref[rowj, rowi], rrefjk=rref[rowi, rowk]))
                     if djk <= cutoff:
                         terms.append(Model_Angle(i, j, k, kangle,
                                      alphaij=alpha[rowi, rowj], alphajk=alpha[rowj, rowk],
                                      rrefij=rref[rowi, rowj], rrefjk=rref[rowj, rowk]))
                 if dik <= cutoff:
                     if djk <= cutoff:
                         terms.append(Model_Angle(i, k, j, kangle,
                                      alphaij=alpha[rowi, rowk], alphajk=alpha[rowk, rowj],
                                      rrefij=rref[rowi, rowk], rrefjk=rref[rowk, rowj]))
                 for l in range(k+1, len(atoms)):
                      dil = d[i,l]
                      djl = d[j,l]
                      dkl = d[k,l]
                      rowl = row(atoms.get_atomic_numbers()[l])-1
                      if dij <= cutoff:
                          if dik <= cutoff and djl <= cutoff:
                              terms.append(Model_Dihedral(k, i, j, l, kdihedral,
                                   alphaij=alpha[rowk, rowi], alphajk=alpha[rowi, rowj], alphakl=alpha[rowj, rowl],
                                   rrefij=rref[rowk, rowi], rrefjk=rref[rowi, rowj], rrefkl=rref[rowj, rowl]))
                          elif dil <= cutoff and djk <= cutoff:
                              terms.append(Model_Dihedral(l, i, j, k, kdihedral,
                                   alphaij=alpha[rowl, rowi], alphajk=alpha[rowi, rowj], alphakl=alpha[rowj, rowk],
                                   rrefij=rref[rowl, rowi], rrefjk=rref[rowi, rowj], rrefkl=rref[rowj, rowk]))
                      if dik <= cutoff:
                          if dij <= cutoff and dkl <= cutoff:
                              terms.append(Model_Dihedral(j, i, k, l, kdihedral,
                                   alphaij=alpha[rowj, rowi], alphajk=alpha[rowi, rowk], alphakl=alpha[rowk, rowl],
                                   rrefij=rref[rowj, rowi], rrefjk=rref[rowi, rowk], rrefkl=rref[rowk, rowl]))
                          elif dil <= cutoff and djk <= cutoff:
                              terms.append(Model_Dihedral(l, i, k, j, kdihedral,
                                   alphaij=alpha[rowl, rowi], alphajk=alpha[rowi, rowk], alphakl=alpha[rowk, rowj],
                                   rrefij=rref[rowl, rowi], rrefjk=rref[rowi, rowk], rrefkl=rref[rowk, rowj]))
                      if dil <= cutoff:
                          if dij <= cutoff and dkl <= cutoff:
                              terms.append(Model_Dihedral(j, i, l, k, kdihedral,
                                   alphaij=alpha[rowj, rowi], alphajk=alpha[rowi, rowl], alphakl=alpha[rowl, rowk],
                                   rrefij=rref[rowj, rowi], rrefjk=rref[rowi, rowl], rrefkl=rref[rowl, rowk]))
                          elif dik <= cutoff and djl <= cutoff:
                              terms.append(Model_Dihedral(k, i, l, j, kdihedral,
                                   alphaij=alpha[rowk, rowi], alphajk=alpha[rowi, rowl], alphakl=alpha[rowl, rowj],
                                   rrefij=rref[rowk, rowi], rrefjk=rref[rowi, rowl], rrefkl=rref[rowl, rowj]))
                      if djk <= cutoff:
                          if dij <= cutoff and dkl <= cutoff:
                              terms.append(Model_Dihedral(i, j, k, l, kdihedral,
                                   alphaij=alpha[rowi, rowj], alphajk=alpha[rowj, rowk], alphakl=alpha[rowk, rowl],
                                   rrefij=rref[rowi, rowj], rrefjk=rref[rowj, rowk], rrefkl=rref[rowk, rowl]))
                          elif djl <= cutoff and dik <= cutoff:
                              terms.append(Model_Dihedral(l, j, k, i, kdihedral,
                                   alphaij=alpha[rowl, rowj], alphajk=alpha[rowj, rowk], alphakl=alpha[rowk, rowi],
                                   rrefij=rref[rowl, rowj], rrefjk=rref[rowj, rowk], rrefkl=rref[rowk, rowi]))
                      if djl <= cutoff:
                          if dij <= cutoff and dkl <= cutoff:
                              terms.append(Model_Dihedral(i, j, l, k, kdihedral,
                                   alphaij=alpha[rowi, rowj], alphajk=alpha[rowj, rowl], alphakl=alpha[rowl, rowk],
                                   rrefij=rref[rowi, rowj], rrefjk=rref[rowj, rowl], rrefkl=rref[rowl, rowk]))
                          elif djk <= cutoff and dil <= cutoff:
                              terms.append(Model_Dihedral(k, j, l, i, kdihedral,
                                   alphaij=alpha[rowk, rowj], alphajk=alpha[rowj, rowl], alphakl=alpha[rowl, rowi],
                                   rrefij=rref[rowk, rowj], rrefjk=rref[rowj, rowl], rrefkl=rref[rowl, rowi]))
                      if dkl <= cutoff:
                          if dik <= cutoff and djl <= cutoff:
                              terms.append(Model_Dihedral(i, k, l, j, kdihedral,
                                   alphaij=alpha[rowi, rowk], alphajk=alpha[rowk, rowl], alphakl=alpha[rowl, rowj],
                                   rrefij=rref[rowi, rowk], rrefjk=rref[rowk, rowl], rrefkl=rref[rowl, rowj]))
                          elif djk <= cutoff and dil <= cutoff:
                              terms.append(Model_Dihedral(j, k, l, i, kdihedral,
                                   alphaij=alpha[rowj, rowk], alphajk=alpha[rowk, rowl], alphakl=alpha[rowl, rowi],
                                   rrefij=rref[rowj, rowk], rrefjk=rref[rowk, rowl], rrefkl=rref[rowl, rowi]))

    return terms


def row(Z):

    if Z <= 2:
        return 1
    elif Z <= 10:
        return 2
    elif Z <= 18:
        return 3
    else:
        return 3

def get_amber_terms(topfile, permutation=None):

    terms = []

    pointers = np.array(get_amber_field(topfile, 'POINTERS'), dtype=int)

    nblist = sparse.lil_matrix((pointers[0], pointers[0]), dtype=bool)

    vdwtypes = np.array(get_amber_field(topfile, 'ATOM_TYPE_INDEX'), dtype=int)
    ntypes = np.max(vdwtypes)
    vdwindexes = np.array(get_amber_field(topfile, 'NONBONDED_PARM_INDEX'), dtype=int)
    vdwaij = np.array(get_amber_field(topfile, 'LENNARD_JONES_ACOEF'), dtype=float)
    vdwbij = np.array(get_amber_field(topfile, 'LENNARD_JONES_BCOEF'), dtype=float)
    charges = np.array(get_amber_field(topfile, 'CHARGE'), dtype=float)/18.2223

    bonds = np.array(get_amber_field(topfile, 'BONDS_INC_HYDROGEN')+get_amber_field(topfile, 'BONDS_WITHOUT_HYDROGEN'), dtype=int).reshape((-1,3))
    k = np.array(get_amber_field(topfile, 'BOND_FORCE_CONSTANT'), dtype=float)
    req = np.array(get_amber_field(topfile, 'BOND_EQUIL_VALUE'), dtype=float)
    for n in range(len(bonds)):
        if permutation is None:
            terms.append(Harmonic_Bond(bonds[n][0]/3, bonds[n][1]/3, k[bonds[n][2]-1]*units.kcal/units.mol*2.0, req[bonds[n][2]-1]))
        else:
            terms.append(Harmonic_Bond(permutation[0,bonds[n][0]/3], permutation[0,bonds[n][1]/3], k[bonds[n][2]-1]*units.kcal/units.mol*2.0, req[bonds[n][2]-1]))
        nblist[bonds[n][0]/3, bonds[n][1]/3] = True
        nblist[bonds[n][1]/3, bonds[n][0]/3] = True

    angles = np.array(get_amber_field(topfile, 'ANGLES_INC_HYDROGEN')+get_amber_field(topfile, 'ANGLES_WITHOUT_HYDROGEN'), dtype=int).reshape((-1,4))
    k = np.array(get_amber_field(topfile, 'ANGLE_FORCE_CONSTANT'), dtype=float)
    theteq = np.array(get_amber_field(topfile, 'ANGLE_EQUIL_VALUE'), dtype=float)
    for n in range(len(angles)):
        if permutation is None:
            terms.append(Harmonic_Angle(angles[n][0]/3, angles[n][1]/3, angles[n][2]/3, k[angles[n][3]-1]*units.kcal/units.mol*2.0, theteq[angles[n][3]-1]))
        else:
            terms.append(Harmonic_Angle(permutation[0,angles[n][0]/3], permutation[0,angles[n][1]/3], permutation[0,angles[n][2]/3], k[angles[n][3]-1]*units.kcal/units.mol*2.0, theteq[angles[n][3]-1]))
        nblist[angles[n][0]/3, angles[n][2]/3] = True
        nblist[angles[n][2]/3, angles[n][0]/3] = True

    dihedrals = np.abs(np.array(get_amber_field(topfile, 'DIHEDRALS_INC_HYDROGEN')+get_amber_field(topfile, 'DIHEDRALS_WITHOUT_HYDROGEN'), dtype=int).reshape((-1,5)))
    phi_k = np.array(get_amber_field(topfile, 'DIHEDRAL_FORCE_CONSTANT'), dtype=float)
    phase = np.array(get_amber_field(topfile, 'DIHEDRAL_PHASE'), dtype=float)
    per = np.array(get_amber_field(topfile, 'DIHEDRAL_PERIODICITY'), dtype=float)
    scee = np.array(get_amber_field(topfile, 'SCEE_SCALE_FACTOR'), dtype=float)
    if len(scee) == 0:
        scee = 1.2*np.ones(len(phi_k))
    scnb = np.array(get_amber_field(topfile, 'SCNB_SCALE_FACTOR'), dtype=float)
    if len(scnb) == 0:
        scnb = 2.0*np.ones(len(phi_k))
    for n in range(len(dihedrals)):
        if permutation is None:
            terms.append(Fourier_Dihedral(dihedrals[n][0]/3, dihedrals[n][1]/3, dihedrals[n][2]/3, dihedrals[n][3]/3, phi_k[dihedrals[n][4]-1]*units.kcal/units.mol, phase[dihedrals[n][4]-1], per[dihedrals[n][4]-1]))
        else:
            terms.append(Fourier_Dihedral(permutation[0,dihedrals[n][0]/3], permutation[0,dihedrals[n][1]/3], permutation[0,dihedrals[n][2]/3], permutation[0,dihedrals[n][3]/3], phi_k[dihedrals[n][4]-1]*units.kcal/units.mol, phase[dihedrals[n][4]-1], per[dihedrals[n][4]-1]))
        if not nblist[dihedrals[n][0]/3, dihedrals[n][3]/3]:
            i = vdwtypes[dihedrals[n][0]/3]
            j = vdwtypes[dihedrals[n][3]/3]
            if i < j:
                k = vdwindexes[ntypes*(i-1)+j-1]-1
            else:
                k = vdwindexes[ntypes*(j-1)+i-1]-1
            if permutation is None:
               terms.append(VdW(dihedrals[n][0]/3, dihedrals[n][3]/3, Aij=vdwaij[k]*units.kcal/units.mol, Bij=vdwbij[k]*units.kcal/units.mol, scale=1.0/scnb[dihedrals[n][4]-1]))
               terms.append(Coulomb(dihedrals[n][0]/3, dihedrals[n][3]/3, chargei=charges[dihedrals[n][0]/3], chargej=charges[dihedrals[n][3]/3], scale=1.0/scee[dihedrals[n][4]-1]))
            else:
               terms.append(VdW(permutation[0,dihedrals[n][0]/3], permutation[0,dihedrals[n][3]/3], Aij=vdwaij[k]*units.kcal/units.mol, Bij=vdwbij[k]*units.kcal/units.mol, scale=1.0/scnb[dihedrals[n][4]-1]))
               terms.append(Coulomb(permutation[0,dihedrals[n][0]/3], permutation[0,dihedrals[n][3]/3], chargei=charges[dihedrals[n][0]/3], chargej=charges[dihedrals[n][3]/3], scale=1.0/scee[dihedrals[n][4]-1]))
        nblist[dihedrals[n][0]/3, dihedrals[n][3]/3] = True
        nblist[dihedrals[n][3]/3, dihedrals[n][0]/3] = True

    for m in range(pointers[0]-1):
        for n in range(m+1, pointers[0]):
            if not nblist[m, n]:
                i = vdwtypes[m]
                j = vdwtypes[n]
                if i < j:
                    k = vdwindexes[ntypes*(i-1)+j-1]-1
                else:
                    k = vdwindexes[ntypes*(j-1)+i-1]-1
                if permutation is None:
                    terms.append(VdW(m, n, Aij=vdwaij[k]*units.kcal/units.mol, Bij=vdwbij[k]*units.kcal/units.mol))
                    terms.append(Coulomb(m, n, chargei=charges[m], chargej=charges[n]))
                else:
                    terms.append(VdW(permutation[0,m], permutation[0,n], Aij=vdwaij[k]*units.kcal/units.mol, Bij=vdwbij[k]*units.kcal/units.mol))
                    terms.append(Coulomb(permutation[0,m], permutation[0,n], chargei=charges[m], chargej=charges[n]))

    return terms

def get_amber_field(topology, name):

    t = open(topology).readlines()
    f = []
    for i in range(len(t)):
        if '%FLAG '+name in t[i]:
            for j in range(i+2, len(t)):
                if t[j][0] == '%':
                    return f
                f += t[j].split()
    return f
